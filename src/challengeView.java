
import java.util.Observable;
import java.util.Observer;
import javax.swing.*;

import java.awt.BorderLayout;
import java.awt.event.*;


public class challengeView extends JFrame implements Observer {
	JPanel myPanel;
	JPanel buttonPanel;
	JLabel info;
	JComboBox challenges;
	JButton button;
	JButton back;
	String item[];
	char chars[];
	String list[];
	String chalID;
	String temp[];
	
	public challengeView(){
		makeInterface();
	}
	
	//create the interface for the challenge screen
	public void makeInterface(){
		myPanel = new JPanel();
		buttonPanel = new JPanel();
		info = new JLabel("Please select a challenge for the Flip 'Em game:");
		challenges = new JComboBox();
		challenges.setActionCommand("combo");
		challenges.setEditable(false);
		button = new JButton("Select");
		button.setActionCommand("Select");
		back = new JButton("Back");
		back.setActionCommand("back");
		
		buttonPanel.setLayout(new BorderLayout());
		myPanel.setLayout(new BorderLayout());
		myPanel.add(info, BorderLayout.NORTH);
		myPanel.add(challenges, BorderLayout.CENTER);
		buttonPanel.add(button, BorderLayout.NORTH);
		buttonPanel.add(back, BorderLayout.SOUTH);
		myPanel.add(buttonPanel, BorderLayout.SOUTH);
		
		this.setTitle("Pick a challenge");
		this.add(myPanel);
		this.pack();
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.getRootPane().setDefaultButton(button);
		UIManager.put("Button.defaultButtonFollowsFocus", Boolean.TRUE);
	}
	
	public void run(){
		this.setVisible(true);
	}
	
	//add the controller to the button and the combobox
	public void addActionController(ActionListener controller){
		challenges.addActionListener(controller);
		button.addActionListener(controller);
		back.addActionListener(controller);
	}
	
	// get the list of challenges for the game
	public void setChallenges(){
		challenges.removeAll();
		int noItems = Integer.parseInt(item[1]);
		for(int i=2; i<noItems+2; i++){
			list = item[i].split("\\s+");
			list[2] = list[2].replace("+", " ");
			challenges.addItem(list[2]);
		}
	}
	
	// get the challenge id for the high scores table
	public String getChalID(int chalNo){
		temp = item[chalNo+2].split("\\s+");
		chalID = temp[0];
		return chalID;
	}
	
	//get the challenges from the model and add them to the combobox
	public void update(Observable arg0, Object arg) {
		if(String[].class == arg.getClass()){
			item = (String[]) arg;
		}
	}

	//close the window when required
	public void closeWindow() {
		this.dispose();
	}

}
