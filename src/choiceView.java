import java.util.Observable;
import java.util.Observer;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class choiceView extends JFrame implements Observer{
	JPanel myPanel;
	JButton flipEm;
	JButton profAl;
	
	
	public choiceView(){
		makeInterface();
	}
	
	public void makeInterface(){
		myPanel = new JPanel();
		myPanel.setLayout(new BorderLayout());
		flipEm = new JButton("The Flip 'Em Game");
		flipEm.setActionCommand("flip");
		profAl = new JButton("Professor Alans Puzzle");
		profAl.setActionCommand("prof");
		
		myPanel.add(flipEm, BorderLayout.NORTH);
		myPanel.add(profAl, BorderLayout.SOUTH);
		
		this.setTitle("Pick a game");
		this.add(myPanel);
		this.pack();
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	public void run(){
		this.setVisible(true);
	}
	
	public void addActionController(ActionListener controller){
		flipEm.addActionListener(controller);
		profAl.addActionListener(controller);
	}
	
	public void closeWindow(){
		this.dispose();
	}
	
	public void update(Observable arg0, Object arg1) {
		
	}

}
