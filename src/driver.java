
public class driver {
	public static void main(String args[]){
		//create an instance of each class
		flipemView flips = new flipemView();
		model model = new model();
		nicknameView nickname = new nicknameView();
		challengeView challenges = new challengeView();
		challengeDescView desc = new challengeDescView();
		choiceView choice = new choiceView();
		profChallengeView profChal = new profChallengeView();
		profAlanView prof = new profAlanView();
		profChallengeDescView profDesc = new profChallengeDescView();
		controller contr = new controller(model, flips, challenges, nickname, desc, choice, profChal, prof, profDesc);
		
		//add the controller to the views
		flips.addActionController(contr);
		flips.addMouseController(contr);
		challenges.addActionController(contr);
		nickname.addActionController(contr);
		desc.addActionController(contr);
		choice.addActionController(contr);
		profChal.addActionController(contr);
		prof.addActionController(contr);
		prof.addMouseController(contr);
		profDesc.addActionController(contr);
		
		//allow the views to observe the model
		model.addObserver(flips);
		model.addObserver(challenges);
		model.addObserver(desc);
		model.addObserver(profDesc);
		model.addObserver(profChal);
		model.addObserver(prof);
		
		contr.run();
	}
}
