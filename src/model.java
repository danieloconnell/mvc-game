import java.util.*;
import java.awt.List;
import java.io.*;
import java.net.URL;
import java.net.URLConnection;

import javax.swing.DefaultListModel;

public class model extends Observable{
	int label[] = new int[2];
	String name;
	String challenge;
	String challengesURL = "http://extrams-web.lancs.ac.uk/252/labs/services/game-service-list.php?";
	String uploadURL = "http://extrams-web.lancs.ac.uk/252/labs/services/game-service-add.php?student_num=9166599&password=yw6l9f&type=Play";
	String scoreURL = "http://extrams-web.lancs.ac.uk/252/labs/services/game-service-list.php?";
	String type = "type=ChallengeList";
	String id = "&id=424";
	String profId = "&id=425";
	String[] chal = new String[10];
	String[] scoreTable = new String[10];
	String[] scoreData = new String[10];
	int choice;
	int score = 510;
	String movesDesc = "hi";
	String[] chalID;
	String forTable;
	Object[] list;
	
	public model(){
		
	}
	
	//method to send though the coordinates of the label to be changed
	public void changeIcon(int x, int y){
		label[0] = x;
		label[1] = y;
		setChanged();
		notifyObservers(label);
	}
	
	public void slide(int a){
		setChanged();
		notifyObservers(a);
	}
	
	//hold the nickname of the current user
	public void nickname(String name){
		this.name = name;
	}
	
	//get the challenges from the progress tracker
	public void getChallenge(int choice){
		this.choice = choice;
		challengesURL += type;
		if(choice == 1){
			challengesURL += id;
		}else if(choice == 2){
			challengesURL += profId;
		}
		
		try {
			URL url = new URL(challengesURL);
			URLConnection connection = url.openConnection();
			connection.connect();
			InputStream in = connection.getInputStream();
			BufferedReader reader = new BufferedReader(new InputStreamReader(in));
			for(int i=0; i<10; i++){
				chal[i] = reader.readLine();
			}
			setChanged();
			notifyObservers(chal);
		} catch (IOException e) {
			System.out.println("No Connection");
		}
	}
	
	//upload the score to the play table 
	public void uploadScore(int moves, int chalNo, String[] movesMade){
		chalID = chal[chalNo+2].split("\\s+");
		
		score = score - (moves*10);
		uploadURL += "&challenge_id="+chalID[0];
		uploadURL += "&name="+name;
		uploadURL += "&score="+score; 
		uploadURL += "&number_moves="+moves;
		uploadURL += "&moves=";
		for(int i=0; i<movesMade.length; i++){
			if(movesMade[i] == null){
			}else{
				uploadURL += movesMade[i] + "+";
			}
		}
		
		forTable = name;
		forTable += " ";
		forTable += Integer.toString(score);
		
		try {
			URL url = new URL(uploadURL);
			URLConnection connection = url.openConnection();
			connection.connect();
			InputStream in = connection.getInputStream();
			setChanged();
			notifyObservers(forTable);
		} catch (IOException e) {
			System.out.println("No connection");
		}
		score = 510;
	}
	
	// get the previous high scores for the challenge
	public void getScores(String chalID){
		scoreURL += "type=PlayList";
		scoreURL += "&id=";
		scoreURL += chalID;		
		
		try {
			URL url = new URL(scoreURL);
			URLConnection connection = url.openConnection();
			connection.connect();
			InputStream in = connection.getInputStream();
			BufferedReader reader = new BufferedReader(new InputStreamReader(in));
			for(int i=0; i<10; i++){
				scoreTable[i] = reader.readLine();
			}
		} catch (IOException e) {
			System.out.println("No connection");
		}
		
		list = new Object[Integer.parseInt(scoreTable[1])];
		
		for(int i=0; i<Integer.parseInt(scoreTable[1]); i++){
			scoreData = scoreTable[i+2].split("\\s+");	
			list[i] = scoreData[2] + " " + scoreData[3];
		}
		setChanged();
		notifyObservers(list);
	}
	
	public void run(){
	}
}
