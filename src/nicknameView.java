import java.awt.event.ActionListener;
import java.awt.BorderLayout;
import java.util.Observable;
import java.util.Observer;
import javax.swing.*;

public class nicknameView extends JFrame implements Observer{

	JPanel myPanel;
	JButton submit;
	JTextField nameField;
	JLabel info;
	
	public nicknameView(){
		makeInterface();
	}
	
	public void makeInterface(){
		myPanel = new JPanel();
		submit = new JButton("Submit");
		nameField = new JTextField();
		info = new JLabel("Please pick a nickname:");
		
		submit.setActionCommand("submit");
		
		myPanel.setLayout(new BorderLayout());
		myPanel.add(info, BorderLayout.NORTH);
		myPanel.add(nameField, BorderLayout.CENTER);
		myPanel.add(submit, BorderLayout.SOUTH);
		
		this.add(myPanel);
		this.setTitle("Set a nickname");
		this.pack();
		this.setSize(250, 100);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.getRootPane().setDefaultButton(submit);
		UIManager.put("Button.defaultButtonFollowsFocus", Boolean.TRUE);
	}
	
	//close the window when required
	public void closeWindow(){
		this.dispose();
	}
	
	public void run(){
		this.setVisible(true);
	}
	
	public void noName(){
		JOptionPane.showMessageDialog(this, "You must enter a nickname", "Pick a nickname", JOptionPane.WARNING_MESSAGE);
	}
	
	//add the controller to the button
	public void addActionController(ActionListener controller){
		submit.addActionListener(controller);
	}

	public void update(Observable arg0, Object arg2) {}
}
