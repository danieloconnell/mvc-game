import java.util.Observable;
import java.util.Observer;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class profAlanView extends JFrame implements Observer{
	JPanel myPanel, buttonPanel, gamePanel;
	JButton reset;
	JButton giveUp;
	JLabel [][] puzzle;
	JLabel [] arrows;
	int x;
	Icon temp;
	String [] start;
	String [] end;
	String [] item;
	String current;
	
	JLabel [] row1;
	JLabel [] row2;
	JLabel [] row3;
	JLabel [] row4;
	JLabel [] column1;
	JLabel [] column2;
	JLabel [] column3;
	JLabel [] column4;
	
	ImageIcon red = new ImageIcon("red.png");
	ImageIcon green = new ImageIcon("green.png");
	ImageIcon blue = new ImageIcon("blue.png");
	ImageIcon yellow = new ImageIcon("yellow.png");
	
	ImageIcon up = new ImageIcon("up.png");
	ImageIcon down = new ImageIcon("down.png");
	ImageIcon left = new ImageIcon("left.png");
	ImageIcon right = new ImageIcon("right.png");
	
	public profAlanView(){
		makeInterface();
	}
	
	public void makeInterface(){
		myPanel = new JPanel();
		gamePanel = new JPanel();
		buttonPanel = new JPanel();
		reset = new JButton("Reset");
		reset.setActionCommand("profReset");
		giveUp = new JButton("Give Up");
		giveUp.setActionCommand("profUp");
		puzzle = new JLabel[6][6];
		
		myPanel.setLayout(new BorderLayout());
		gamePanel.setLayout(new GridLayout(6, 6));
		buttonPanel.setLayout(new BorderLayout());
		
		for(int j=0; j<6; j++){
			for(int i=0; i<6; i++){
				puzzle[j][i] = new JLabel();
				gamePanel.add(puzzle[j][i]);
				
				if(j == 1 && (i==1||i==2||i==3||i==4)){
					puzzle[j][i].setIcon(red);
				}else if(j == 2 && (i==1||i==2||i==3||i==4)){
					puzzle[j][i].setIcon(green);
				}else if(j == 3 && (i==1||i==2||i==3||i==4)){
					puzzle[j][i].setIcon(blue);
				}else if(j == 4 && (i==1||i==2||i==3||i==4)){
					puzzle[j][i].setIcon(yellow);
				}else if(j == 0 && (i==1||i==2||i==3||i==4)){
					puzzle[j][i].setIcon(up);
				}else if(j == 5 && (i==1||i==2||i==3||i==4)){
					puzzle[j][i].setIcon(down);
				}else if(i == 0 && (j==1||j==2||j==3||j==4)){
					puzzle[j][i].setIcon(left);
				}else if(i == 5 && (j==1||j==2||j==3||j==4)){
					puzzle[j][i].setIcon(right);
				}
			}
		}
		
		arrows = new JLabel[16];
		arrows[0] = puzzle[0][1];
		arrows[1] = puzzle[0][2];
		arrows[2] = puzzle[0][3];
		arrows[3] = puzzle[0][4];
		arrows[4] = puzzle[5][1];
		arrows[5] = puzzle[5][2];
		arrows[6] = puzzle[5][3];
		arrows[7] = puzzle[5][4];
		arrows[8] = puzzle[1][0];
		arrows[9] = puzzle[2][0];
		arrows[10] = puzzle[3][0];
		arrows[11] = puzzle[4][0];
		arrows[12] = puzzle[1][5];
		arrows[13] = puzzle[2][5];
		arrows[14] = puzzle[3][5];
		arrows[15] = puzzle[4][5];
		
		buttonPanel.add(reset, BorderLayout.NORTH);
		buttonPanel.add(giveUp, BorderLayout.SOUTH);
		
		myPanel.add(gamePanel, BorderLayout.NORTH);
		myPanel.add(buttonPanel, BorderLayout.SOUTH);
		
		this.add(myPanel);
		this.setTitle("Professor Alans Puzzle");
		this.pack();
		this.setSize(405, 450);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	// add the action listener to the buttons
	public void addActionController(ActionListener controller){
		reset.addActionListener(controller);
		giveUp.addActionListener(controller);
	}
	
	// add action listeners to all of the labels with an arrow
	public void addMouseController(MouseListener controller){
		puzzle[0][1].addMouseListener(controller);
		puzzle[0][2].addMouseListener(controller);
		puzzle[0][3].addMouseListener(controller);
		puzzle[0][4].addMouseListener(controller);
		puzzle[5][1].addMouseListener(controller);
		puzzle[5][2].addMouseListener(controller);
		puzzle[5][3].addMouseListener(controller);
		puzzle[5][4].addMouseListener(controller);
		puzzle[1][0].addMouseListener(controller);
		puzzle[2][0].addMouseListener(controller);
		puzzle[3][0].addMouseListener(controller);
		puzzle[4][0].addMouseListener(controller);
		puzzle[1][5].addMouseListener(controller);
		puzzle[2][5].addMouseListener(controller);
		puzzle[3][5].addMouseListener(controller);
		puzzle[4][5].addMouseListener(controller);
	}
	
	// this method slides the row or column
	public void slide(){
		if(x==0||x==1||x==2||x==3){
			temp = puzzle[1][x+1].getIcon();
		}
		
		if(x==4||x==5||x==6||x==7){
			temp = puzzle[4][x-3].getIcon();
		}
		
		if(x==8||x==9||x==10||x==11){
			temp = puzzle[x-7][1].getIcon();
		}
		
		if(x==12||x==13||x==14||x==15){
			temp = puzzle[x-11][4].getIcon();
		}
		
		for(int i=1; i<5; i++){
			if(x==0){
				if(i+1==5){
					puzzle[i][1].setIcon(temp);
				}else{
					puzzle[i][1].setIcon(puzzle[i+1][1].getIcon());
				}
			}else if(x==1){
				if(i+1==5){
					puzzle[i][2].setIcon(temp);
				}else{
					puzzle[i][2].setIcon(puzzle[i+1][2].getIcon());
				}
			}else if(x==2){
				if(i+1==5){
					puzzle[i][3].setIcon(temp);
				}else{
					puzzle[i][3].setIcon(puzzle[i+1][3].getIcon());
				}
			}else if(x==3){
				if(i+1==5){
					puzzle[i][4].setIcon(temp);
				}else{
					puzzle[i][4].setIcon(puzzle[i+1][4].getIcon());
				}
			}
		}
		
		for(int i=4;i>0;i--){
			if(x==4){
				if(i==1){
					puzzle[i][1].setIcon(temp);
				}else{
					puzzle[i][1].setIcon(puzzle[i-1][1].getIcon());
				}
			}else if(x==5){
				if(i==1){
					puzzle[i][2].setIcon(temp);
				}else{
					puzzle[i][2].setIcon(puzzle[i-1][2].getIcon());
				}
			}else if(x==6){
				if(i==1){
					puzzle[i][3].setIcon(temp);
				}else{
					puzzle[i][3].setIcon(puzzle[i-1][3].getIcon());
				}
			}else if(x==7){
				if(i==1){
					puzzle[i][4].setIcon(temp);
				}else{
					puzzle[i][4].setIcon(puzzle[i-1][4].getIcon());
				}
			}
		}
		
		for(int i=1; i<5; i++){
			if(x==8){
				if(i+1==5){
					puzzle[1][i].setIcon(temp);
				}else{
					puzzle[1][i].setIcon(puzzle[1][i+1].getIcon());
				}
			}else if(x==9){
				if(i+1==5){
					puzzle[2][i].setIcon(temp);
				}else{
					puzzle[2][i].setIcon(puzzle[2][i+1].getIcon());
				}
			}else if(x==10){
				if(i+1==5){
					puzzle[3][i].setIcon(temp);
				}else{
					puzzle[3][i].setIcon(puzzle[3][i+1].getIcon());
				}
			}else if(x==11){
				if(i+1==5){
					puzzle[4][i].setIcon(temp);
				}else{
					puzzle[4][i].setIcon(puzzle[4][i+1].getIcon());
				}
			}
		}
		
		for(int i=4;i>0;i--){
			if(x==12){
				if(i==1){
					puzzle[1][i].setIcon(temp);
				}else{
					puzzle[1][i].setIcon(puzzle[1][i-1].getIcon());
				}
			}else if(x==13){
				if(i==1){
					puzzle[2][i].setIcon(temp);
				}else{
					puzzle[2][i].setIcon(puzzle[2][i-1].getIcon());
				}
			}else if(x==14){
				if(i==1){
					puzzle[3][i].setIcon(temp);
				}else{
					puzzle[3][i].setIcon(puzzle[3][i-1].getIcon());
				}
			}else if(x==15){
				if(i==1){
					puzzle[4][i].setIcon(temp);
				}else{
					puzzle[4][i].setIcon(puzzle[4][i-1].getIcon());
				}
			}
		}
	}
	
	public void run(){
		this.setVisible(true);
	}
	
	// reset the squares
	public void reset(){
		for(int j=0; j<6; j++){
			for(int i=0; i<6; i++){
				if(j == 1 && (i==1||i==2||i==3||i==4)){
					puzzle[j][i].setIcon(red);
				}else if(j == 2 && (i==1||i==2||i==3||i==4)){
					puzzle[j][i].setIcon(green);
				}else if(j == 3 && (i==1||i==2||i==3||i==4)){
					puzzle[j][i].setIcon(blue);
				}else if(j == 4 && (i==1||i==2||i==3||i==4)){
					puzzle[j][i].setIcon(yellow);
				}else if(j == 0 && (i==1||i==2||i==3||i==4)){
					puzzle[j][i].setIcon(up);
				}else if(j == 5 && (i==1||i==2||i==3||i==4)){
					puzzle[j][i].setIcon(down);
				}else if(i == 0 && (j==1||j==2||j==3||j==4)){
					puzzle[j][i].setIcon(left);
				}else if(i == 5 && (j==1||j==2||j==3||j==4)){
					puzzle[j][i].setIcon(right);
				}
			}
		}
	}
	
	public void closeWindow(){
		this.dispose();
	}
	
	// check if the game has been completed
	public boolean checkEnd(){
		boolean outcome = false;
		current = "";
		
		for(int j=0; j<6; j++){
			for(int i=0; i<6; i++){
				if(puzzle[i][j].getIcon() == red){
					current += 'R';
				}else if(puzzle[i][j].getIcon() == green){
					current += 'Y';
				}else if(puzzle[i][j].getIcon() == blue){
					current += 'B';
				}else if(puzzle[i][j].getIcon() == yellow){
					current += 'G';
				}
			}
		}
		
		if(current.equals(end[5])){
			outcome = true;
		}
	
		return outcome;
	}
	
	// get the start and end states of the chllenge
	public void getStartAndEnd(int chalNo){
		start = item[2].split("\\s+");
		end = item[2].split("\\s+");
		start[4] = start[4].replace("+", "");
		end[5] = end[5].replace("+", "");
	}
	
	public void update(Observable arg0, Object arg) {
		if(Integer.class == arg.getClass()){
			x = (int)arg;
		}else if(String[].class == arg.getClass()){
			item = (String[]) arg;
		}
	}

}
