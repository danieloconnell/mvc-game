import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.*;
import java.util.*;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class challengeDescView extends JFrame implements Observer{
	JPanel myPanel, buttonPanel, tablePanel, previewPanel, bottomPanel;
	JButton play;
	JButton challenges;
	JLabel desc;
	JTable score;
	DefaultTableModel model;
	String [] item;
	String [] chalDesc;
	String[] forTable = new String[2];
	String temp;
	int chalNo;
	Object[] scoreData;
	char [] end = new char[16];
	JLabel[] preview;
	
	//get the gifs for coins
	ImageIcon heads = new ImageIcon("cointail.gif");
	ImageIcon tails = new ImageIcon("coinhead.gif");
	
	public challengeDescView(){
		makeInterface();
	}
	
	public void run(){
		this.setVisible(true);
	}
	
	//create the interface for the description view 
	public void makeInterface(){
		myPanel = new JPanel();
		buttonPanel = new JPanel();
		tablePanel = new JPanel();
		previewPanel = new JPanel();
		bottomPanel = new JPanel();
		model = new DefaultTableModel();
		desc = new JLabel();
		play = new JButton("Play Game");
		play.setActionCommand("play");
		challenges = new JButton("Challenges");
		challenges.setActionCommand("challenges");
		model.addColumn("Nickname");
		model.addColumn("Score");
		score = new JTable(model);
		preview = new JLabel[16];
		
		previewPanel.setLayout(new GridLayout(4,4));
		for(int i=0; i<16; i++){
			preview[i] = new JLabel();
			previewPanel.add(preview[i]);
		}
		
		myPanel.setLayout(new BorderLayout());
		buttonPanel.setLayout(new BorderLayout());
		tablePanel.setLayout(new BorderLayout());
		bottomPanel.setLayout(new BorderLayout());
		buttonPanel.add(challenges, BorderLayout.NORTH);
		buttonPanel.add(play, BorderLayout.SOUTH);
		myPanel.add(desc, BorderLayout.NORTH);
		tablePanel.add(score.getTableHeader(), BorderLayout.NORTH);
		tablePanel.add(score, BorderLayout.CENTER);
		myPanel.add(tablePanel, BorderLayout.CENTER);
		bottomPanel.add(previewPanel, BorderLayout.CENTER);
		bottomPanel.add(buttonPanel, BorderLayout.SOUTH);
		myPanel.add(bottomPanel, BorderLayout.SOUTH);
		
		this.add(myPanel);
		this.setTitle("Challenge description");
		this.pack();
		this.setSize(250, 350);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	//add the controller to the buttons
	public void addActionController(ActionListener controller){
		play.addActionListener(controller);
		challenges.addActionListener(controller);
	}
	
	//close the window when called
	public void closeWindow(){
		this.dispose();
	}
	
	// set the challenge description
	public void setChallenge(int chalNo){
		this.chalNo = chalNo;
		chalDesc = item[chalNo+2].split("\\s+");
		chalDesc[3] = chalDesc[3].replace("+", " ");
		desc.setText(chalDesc[3]);
		getPreview();
	}
	
	// add the previous high scores to the table
	public void addToTable(){
		for(int i=0; i<scoreData.length; i++){
			forTable = ((String) scoreData[i]).split("\\s+");
			forTable[0] = forTable[0].replace("+", " ");
			model.addRow(new Object[]{forTable[0], forTable[1]});
		}
	}
	
	// show the preview for the end state of the challenge
	public void getPreview(){
		chalDesc[5] = chalDesc[5].replace("+", "");
		end = chalDesc[5].toCharArray();
		
		for(int i=0; i<16; i++){
			if(end[i] == 'H'){
				preview[i].setIcon(heads);
			}else if(end[i] == 'T'){
				preview[i].setIcon(tails);
			}
		}
	}
	
	//get the challenge description from the model
	public void update(Observable arg0, Object arg) {
		if(String[].class == arg.getClass()){
			item = (String[]) arg;
		}else if(Object[].class == arg.getClass()){
			scoreData = (Object[]) arg;
			addToTable();
		}
	}
}
