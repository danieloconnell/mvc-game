import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class profChallengeDescView extends JFrame implements Observer{
	JPanel myPanel, buttonPanel, tablePanel, previewPanel, bottomPanel;
	JButton play;
	JButton challenges;
	JLabel desc;
	JTable score;
	DefaultTableModel model;
	String [] item;
	String [] chalDesc;
	int chalNo;
	String chalID;
	String temp;
	String [] forTable;
	Object[] scoreData;
	JLabel[] preview;
	char [] end = new char[16];
	
	ImageIcon red = new ImageIcon("red.png");
	ImageIcon green = new ImageIcon("green.png");
	ImageIcon blue = new ImageIcon("blue.png");
	ImageIcon yellow = new ImageIcon("yellow.png");
	
	public profChallengeDescView(){
		makeInterface();
	}
	
	public void run(){
		this.setVisible(true);
	}
	
	//create the interface for the description view 
	public void makeInterface(){
		myPanel = new JPanel();
		buttonPanel = new JPanel();
		tablePanel = new JPanel();
		previewPanel = new JPanel();
		bottomPanel = new JPanel();
		desc = new JLabel();
		play = new JButton("Play Game");
		play.setActionCommand("profPlay");
		challenges = new JButton("Challenges");
		challenges.setActionCommand("profChallenges");
		model = new DefaultTableModel();
		score = new JTable(model);
		preview = new JLabel[16];
		
		previewPanel.setLayout(new GridLayout(4,4));
		for(int i=0; i<16; i++){
			preview[i] = new JLabel();
			previewPanel.add(preview[i]);
		}
		
		model.addColumn("Nickname");
		model.addColumn("Score");
		
		myPanel.setLayout(new BorderLayout());
		buttonPanel.setLayout(new BorderLayout());
		tablePanel.setLayout(new BorderLayout());
		bottomPanel.setLayout(new BorderLayout());
		myPanel.add(desc, BorderLayout.NORTH);
		tablePanel.add(score.getTableHeader(), BorderLayout.NORTH);
		tablePanel.add(score, BorderLayout.CENTER);
		myPanel.add(tablePanel, BorderLayout.CENTER);
		buttonPanel.add(challenges, BorderLayout.NORTH);
		buttonPanel.add(play, BorderLayout.SOUTH);
		bottomPanel.add(previewPanel, BorderLayout.CENTER);
		bottomPanel.add(buttonPanel, BorderLayout.SOUTH);
		myPanel.add(bottomPanel, BorderLayout.SOUTH);
		
		this.add(myPanel);
		this.setTitle("Challenge description");
		this.pack();
		this.setSize(250, 450);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	//add the controller to the buttons
	public void addActionController(ActionListener controller){
		play.addActionListener(controller);
		challenges.addActionListener(controller);
	}
	
	//close the window when called
	public void closeWindow(){
		this.dispose();
	}
	
	// get the description for the challenge
	public void setChallenge(int chalNo){
		this.chalNo = chalNo;
		chalDesc = item[chalNo+2].split("\\s+");
		chalDesc[3] = chalDesc[3].replace("+", " ");
		desc.setText(chalDesc[3]);
		chalID = chalDesc[0];
		getPreview();
	}
	
	// add previous high scores to the table
	public void addToTable(){
		for(int i=0; i<scoreData.length; i++){
			forTable = ((String) scoreData[i]).split("\\s+");
			forTable[0] = forTable[0].replace("+", " ");
			model.addRow(new Object[]{forTable[0], forTable[1]});
		}
	}
	
	// show the preview for the end state of the challenge
	public void getPreview(){
		chalDesc[5] = chalDesc[5].replace("+", "");
		end = chalDesc[5].toCharArray();
		
		for(int i=0; i<16; i++){
			if(end[i] == 'R'){
				preview[i].setIcon(red);
			}else if(end[i] == 'G'){
				preview[i].setIcon(green);
			}else if(end[i] == 'Y'){
				preview[i].setIcon(yellow);
			}else if(end[i] == 'B'){
				preview[i].setIcon(blue);
			}
		}
	}
	
	//get the challenge description from the model
	public void update(Observable arg0, Object arg) {
		if(String[].class == arg.getClass()){
			item = (String[]) arg;
		}else if(Object[].class == arg.getClass()){
			scoreData = (Object[]) arg;
			addToTable();
		}
	}
}
