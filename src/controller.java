import java.awt.event.*;


public class controller implements ActionListener, MouseListener {
	private flipemView flips;
	private model model;
	private nicknameView nickname;
	private challengeView challenges;
	private challengeDescView desc;
	private choiceView choice;
	private profAlanView prof;
	private profChallengeView profChallenges;
	private profChallengeDescView profDesc;
	int x;
	int y;
	int a;
	int moves = 0;
	int chalNo;
	String[] movesMade = new String[10];
	int k = 0;
	int m = 0;
	
	//get all the other classes
	public controller(model m, flipemView f, challengeView c, nicknameView n, challengeDescView d, choiceView o, profChallengeView pc,
			profAlanView p, profChallengeDescView pd){
		this.model = m;
		this.flips = f;
		this.nickname = n;
		this.challenges = c;
		this.desc = d;
		this.choice = o;
		this.prof = p;
		this.profChallenges = pc;
		this.profDesc = pd;
	}
	
	//What to do when the buttons etc. from other classes are pressed
	public void actionPerformed(ActionEvent e) {	
		if(e.getActionCommand().equals("Reset")){
			flips.reset();
		}else if(e.getActionCommand().equals("submit")){
			if(nickname.nameField.getText().equals("")){
				nickname.noName();
			}else{
				model.nickname(nickname.nameField.getText());
				nickname.closeWindow();
				choice.run();
			}
		}else if(e.getActionCommand().equals("Select")){
			challenges.closeWindow();
			chalNo = challenges.challenges.getSelectedIndex();
			model.getScores(challenges.getChalID(chalNo));
			desc.run();
			desc.setChallenge(chalNo);
		}else if(e.getActionCommand().equals("play")){
			desc.closeWindow();
			flips.getStartAndEnd(chalNo);
			flips.run();
			moves = 0;
		}else if(e.getActionCommand().equals("challenges")){
			desc.closeWindow();
			challenges.run();
		}else if(e.getActionCommand().equals("up")){
			flips.closeWindow();
			model.getScores(challenges.getChalID(chalNo));
			desc.run();
		}else if(e.getActionCommand().equals("flip")){
			choice.closeWindow();
			model.getChallenge(1);
			challenges.run();
			challenges.setChallenges();
		}else if(e.getActionCommand().equals("prof")){
			choice.closeWindow();
			model.getChallenge(2);
			profChallenges.run();
			profChallenges.setChallenges();
		}else if(e.getActionCommand().equals("back")){
			challenges.closeWindow();
			choice.run();
		}else if(e.getActionCommand().equals("profBack")){
			profChallenges.closeWindow();
			choice.run();
		}else if(e.getActionCommand().equals("profSelect")){
			profChallenges.closeWindow();
			chalNo = profChallenges.challenges.getSelectedIndex();
			model.getScores(challenges.getChalID(chalNo));
			profDesc.run();
			profDesc.setChallenge(chalNo);
		}else if(e.getActionCommand().equals("profUp")){
			prof.closeWindow();
			model.getScores(challenges.getChalID(chalNo));
			profDesc.run();
		}else if(e.getActionCommand().equals("profChallenges")){
			profDesc.closeWindow();
			profChallenges.run();
		}else if(e.getActionCommand().equals("profPlay")){
			profDesc.closeWindow();
			prof.getStartAndEnd(chalNo);
			moves = 0;
			prof.run();
		}else if(e.getActionCommand().equals("profReset")){
			prof.reset();
		}
	}
	
	//When the label is clicked this is run
	public void mouseClicked(MouseEvent arg) {
		if(model.choice == 1){
			for(int i=0; i<4;i++){
				for(int j=0; j<4;j++){
					if(flips.label[i][j].equals(arg.getSource())){
						x = i;
						y = j;
						movesMade[k] = x + "+" + y;
						k++;
						model.changeIcon(x, y);
						flips.changeIcon();
						moves++;
					}
				}
			}
			if(flips.checkEnd() == true){
				model.uploadScore(moves, chalNo, movesMade);
				flips.closeWindow();
				desc.run();
			}
		}else if(model.choice ==2){
			for(int k=0; k<16;k++){
				if(prof.arrows[k].equals(arg.getSource())){
					a = k;
					movesMade[m] = Integer.toString(k);
					m++;
					model.slide(a);
					prof.slide();
					moves++;
				}
			}
			if(prof.checkEnd() == true){
				model.uploadScore(moves, chalNo, movesMade);
				prof.closeWindow();
				profDesc.run();
			}
		}
	}
	
	public void run(){
		model.run();
		nickname.run();
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}
