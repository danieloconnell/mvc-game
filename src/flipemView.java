import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import java.util.Observable;
import java.util.Observer;

public class flipemView extends JFrame implements MouseListener, Observer{
	boolean [][] grid;
	JLabel [][] label;
	JButton reset;
	JButton giveUp;
	JPanel myPanel;
	JPanel labelPanel;
	JPanel buttonPanel;
	int x;
	int y;
	int [] temp = new int[2];
	String [] item;
	String [] start;
	char [] initial = new char[30];
	char [] endTemp;
	String [] end;
	String current;
	
	//get the gifs for coins
	ImageIcon heads = new ImageIcon("cointail.gif");
	ImageIcon tails = new ImageIcon("coinhead.gif");
	
	public flipemView(){
		makeInterface();
	}
	
	public void makeInterface(){
		myPanel = new JPanel();
		labelPanel = new JPanel();
		buttonPanel = new JPanel();
		label = new JLabel[5][5];
		reset = new JButton("Reset");
		reset.setActionCommand("Reset");
		giveUp = new JButton("Give Up");
		giveUp.setActionCommand("up");
		
		labelPanel.setLayout(new GridLayout(5,4));
		for(int i = 0;i<4;i++){
			for(int j = 0;j<4;j++){
				label[i][j]= new JLabel(heads, JLabel.CENTER);
				labelPanel.add(label[i][j]);
			}
		}
		
		myPanel.setLayout(new BorderLayout());
		myPanel.add(labelPanel, BorderLayout.NORTH);
		myPanel.add(reset, BorderLayout.CENTER);
		myPanel.add(giveUp, BorderLayout.SOUTH);
		
		this.add(myPanel);
		this.pack();
		this.setSize(180,250);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	//method to change the coin icon when it has been clicked
	public void changeIcon(){
		if(label[x][y].getIcon() != tails)label[x][y].setIcon(tails);else label[x][y].setIcon(heads);
		
		if(x+1 == 4){
			if(label[0][y].getIcon() != tails)label[0][y].setIcon(tails);else label[0][y].setIcon(heads);
		}else{
			if(label[x+1][y].getIcon() != tails)label[x+1][y].setIcon(tails);else label[x+1][y].setIcon(heads);
		}
		
		if(x-1 == -1){
			if(label[3][y].getIcon() != tails)label[3][y].setIcon(tails);else label[3][y].setIcon(heads); 
		}else{
			if(label[x-1][y].getIcon() != tails)label[x-1][y].setIcon(tails);else label[x-1][y].setIcon(heads); 
		}
		
		if(y+1 == 4){
			if(label[x][0].getIcon() != tails)label[x][0].setIcon(tails);else label[x][0].setIcon(heads);
		}else{
			if(label[x][y+1].getIcon() != tails)label[x][y+1].setIcon(tails);else label[x][y+1].setIcon(heads);
		}
		
		if(y-1 == -1){
			if(label[x][3].getIcon() != tails)label[x][3].setIcon(tails);else label[x][3].setIcon(heads); 
		}else{
			if(label[x][y-1].getIcon() != tails)label[x][y-1].setIcon(tails);else label[x][y-1].setIcon(heads); 
		}
	}
	
	//set all the coins to the initial state when the button is clicked
	public void reset(){
		this.initialise();
	}
	
	public void run(){
		this.setVisible(true);
	}
	
	//add the controller to all of the labels
	public void addMouseController(MouseListener controller){
		for(int i=0; i<4; i++){
			for(int j=0; j<4; j++){
				label[i][j].addMouseListener(controller);
			}
		}
	}
	
	//add the controller to the buttons
	public void addActionController(ActionListener controller){
		reset.addActionListener(controller);
		giveUp.addActionListener(controller);
	}
	
	//close the window when required
	public void closeWindow(){
		this.dispose();
	}

	//get the initial state and the coordinates of clicked labels from the model
	public void update(Observable arg0, Object arg) {
		try{
			if(int[].class == arg.getClass()){
				temp = (int[])arg;
				x = temp[0];
				y = temp[1];
			}else if(String[].class == arg.getClass()){
				item = (String[]) arg;
			}
		}catch(NullPointerException n){
			
		}
	}
	
	// get the start and end states
	public void getStartAndEnd(int chalNo){
		start = item[chalNo+2].split("\\s+");
		end = item[chalNo+2].split("\\s+");
		start[4] = start[4].replace("+", "");
		end[5] = end[5].replace("+", "");
		initialise();
	}
	
	//set all the coins according to the pattern given in the challenge
	public void initialise(){
		initial = start[4].toCharArray();
		for(int i=0; i<4; i++){
			if(initial[i] == 'H')label[0][i].setIcon(heads);else label[0][i].setIcon(tails);
		}
		
		for(int i=4; i<8; i++){
			if(initial[i] == 'H')label[1][i-4].setIcon(heads);else label[1][i-4].setIcon(tails);
		}
		
		for(int i=8; i<12; i++){
			if(initial[i] == 'H')label[2][i-8].setIcon(heads);else label[2][i-8].setIcon(tails);
		}
		
		for(int i=12; i<16; i++){
			if(initial[i] == 'H')label[3][i-12].setIcon(heads);else label[3][i-12].setIcon(tails);
		}
	}

	//check the end state against the pattern from the challenge
	public boolean checkEnd(){
		current = "";
		boolean outcome = false;
		int l = 0;
		for(int j=0; j<4; j++){
			for(int i=0; i<4; i++){
				if(label[i][j].getIcon() == heads){
					current += 'H';
				}else{
					current += 'T';
				}
			}
		}
		
		if(current.equals(end[5])){
			outcome = true;
		}
	
		return outcome;
	}
	
	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}
